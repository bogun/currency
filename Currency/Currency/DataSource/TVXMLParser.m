//
//  TVXMLParser.m
//  Currency
//
//  Created by Valentin Titov on 09.07.16.
//  Copyright © 2016 Valentin Titov. All rights reserved.
//

#import "TVXMLParser.h"
#import "TVCurrencyModel.h"
//#import "TVUIConst.h"

static NSString* const kDefaultURL = @"http://www.ecb.europa.eu/stats/eurofxref/eurofxref-daily.xml";
static NSString * const kCubeElementRoot = @"Cube";
static NSString * const kAttributeCurrency = @"currency";
static NSString * const kAttributeRate = @"rate";


@interface TVXMLParser () <NSXMLParserDelegate> {
/** Main parser */
    NSXMLParser *_parser;
    NSArray *_arrCurrency;
    NSMutableArray *_parsedElements;
/** Detected Cube currency */
    BOOL _seekCube;
/** Detected currency */
    BOOL _seekCurrency;
/** Detected rate */
    BOOL _seekRate;
/** Currency model */    
    TVCurrencyModel* _currencyModel;
}

@end

@implementation TVXMLParser

- (instancetype)initWithCurrency:(NSArray*) arrCurrency
{
    self = [super init];
    if (self) {
        [self setCurrency:arrCurrency];
    }
    return self;
}

- (void) setCurrency:(NSArray*)arrCurrency {
    _arrCurrency = arrCurrency;
}


- (void) startParse {
    _parsedElements = [[NSMutableArray alloc] init];
    NSURL *urlPath = [NSURL URLWithString:kDefaultURL];
    NSData *urlData = [NSData dataWithContentsOfURL:urlPath];
    [self initParserWithData:urlData];
}

- (void) initParserWithData:(NSData*) data {
    _parser = [[NSXMLParser alloc] initWithData:data];
    _parser.delegate = self;
    [_parser parse];
}


#pragma mark - NSXmlParserDelegate

- (void)parser:(NSXMLParser *)parser
didStartElement:(NSString *)elementName
  namespaceURI:(NSString *)namespaceURI
 qualifiedName:(NSString *)qName
    attributes:(NSDictionary<NSString *, NSString *> *)attributeDict {

    if ([elementName isEqualToString:kCubeElementRoot] && attributeDict[kAttributeCurrency]) {
        if ([_arrCurrency containsObject:attributeDict[kAttributeCurrency]]) {
            TVCurrencyModel *currencyModel = [[TVCurrencyModel alloc] init];
            currencyModel.currency = attributeDict[kAttributeCurrency];
            currencyModel.rate = [attributeDict[kAttributeRate] floatValue];
            [_parsedElements addObject:currencyModel];
        }
    }
}


- (void)parserDidEndDocument:(NSXMLParser *)parser {
    [self.dataSource didLoadCurrencyRate:_parsedElements];
}

@end
