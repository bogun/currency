//
//  TVCurrencyModel.m
//  Currency
//
//  Created by Valentin Titov on 09.07.16.
//  Copyright © 2016 Valentin Titov. All rights reserved.
//

#import "TVCurrencyModel.h"

@implementation TVCurrencyModel

- (NSString *)description {
    return [NSString stringWithFormat:@"%@ -> %f", self.currency, self.rate];
}

@end
