//
//  TVDataSourceConst.h
//  Currency
//
//  Created by Valentin Titov on 18.07.16.
//  Copyright © 2016 Valentin Titov. All rights reserved.
//

#ifndef TVDataSourceConst_h
#define TVDataSourceConst_h
/* The minimum user balance */
static CGFloat const kMinExchangeBalance = 100.0f;
/* The time interfal for update currency rates */
static CGFloat const kUpdateCurrencyTimeInterval = 30.0f;
/* Default currency from */
static NSString* const kDefaultCurrencyFrom = @"EUR";
/* Default currency to */
static NSString* const kDefaultCurrencyTo = @"USD";
/* Count of showed cell. In current version must have value only "2" */
static NSInteger const kCurrencyViewCellsCount = 2;



#endif /* TVDataSourceConst_h */
