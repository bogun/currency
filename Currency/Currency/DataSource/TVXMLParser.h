//
//  TVXMLParser.h
//  Currency
//
//  Created by Valentin Titov on 09.07.16.
//  Copyright © 2016 Valentin Titov. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol TVXMLParserDataSource <NSObject>
@required
/*  Will called when document load from internet and finish parsing
 *  @param currencyRate An array with all parsed currency rates
 */
- (void) didLoadCurrencyRate:(NSArray*) currencyRate;

@end

@interface TVXMLParser : NSObject

@property (assign, nonatomic) id<TVXMLParserDataSource> dataSource;

/** Init parser with user currency list
 *  @param arrCurrency An array of user currency list
 *  @return instance of class
 */
- (instancetype)initWithCurrency:(NSArray*) arrCurrency;
/** Start parsing document */
- (void) startParse;

@end
