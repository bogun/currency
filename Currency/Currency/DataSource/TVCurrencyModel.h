//
//  TVCurrencyModel.h
//  Currency
//
//  Created by Valentin Titov on 09.07.16.
//  Copyright © 2016 Valentin Titov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TVCurrencyModel : NSObject


@property(strong, nonatomic) NSString *currency;
@property(assign, nonatomic) CGFloat rate;

@end
