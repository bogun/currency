//
//  TVCurrencyManager.m
//  Currency
//
//  Created by Valentin Titov on 08.07.16.
//  Copyright © 2016 Valentin Titov. All rights reserved.
//
#import <UIKit/UIKit.h>
#import "TVCurrencyManager.h"
#import "TVCurrencyModel.h"
@interface TVCurrencyManager () {
    NSMutableArray *_currencyRateVariants;
    NSString* _pathToUserBalance;
    NSMutableDictionary *_userBalanceInfo;
}

@end

@implementation TVCurrencyManager

+ (instancetype) currencyManager {
    static TVCurrencyManager *_shared = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _shared = [[TVCurrencyManager alloc] init];
    });
    return _shared;
}

- (instancetype)init {
    self = [super init];
    if (self) {
        [self loadUserBalance];
    }
    return self;
}

- (void) loadUserBalance {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0]; // Get documents folder
    _pathToUserBalance = [documentsDirectory stringByAppendingPathComponent:@"UserBalance.plist"];
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if (![fileManager fileExistsAtPath:_pathToUserBalance]) {
        NSError *error = nil;
        NSString *pathForResource = [[NSBundle mainBundle] pathForResource:@"UserBalance" ofType:@"plist"];
        if (![fileManager copyItemAtPath:pathForResource toPath:_pathToUserBalance error:&error]) {
            NSLog(@"Error!: %@",error);
        }
    }
    _userBalanceInfo = [[NSMutableDictionary alloc] initWithContentsOfFile:_pathToUserBalance];
}


- (NSArray*) userCurrency {
    return [_userBalanceInfo allKeys];
}

- (NSDictionary*) userBalanceInfo {
    return _userBalanceInfo;
}

- (NSInteger) count {
    return [_userBalanceInfo allKeys].count;
}

-(NSArray *)fullInfoForCurrencyRates:(NSArray *)currencyRates {
    _currencyRateVariants = [NSMutableArray array];
    NSArray *currencyList = [_userBalanceInfo allKeys];
    for (NSString *currencyFrom in currencyList) {
        for (NSString *currencyTo in currencyList) {
            if (![currencyFrom isEqualToString:currencyTo]) {
                [_currencyRateVariants addObject:[self makePairForCurrencyFrom:currencyFrom
                                                                   currencyTo:currencyTo
                                                                currencyRates:currencyRates]];
            }
        }
    }
    return _currencyRateVariants;
}

- (NSDictionary *) makePairForCurrencyFrom:(NSString*)currencyFrom
                                currencyTo:(NSString*)currencyTo
                             currencyRates:(NSArray*)currencyRates {
    
    NSDictionary *currencyInfoFrom = _userBalanceInfo[currencyFrom];
    NSDictionary *currencyInfoTo = _userBalanceInfo[currencyTo];
    NSInteger valueFrom = 1;
    NSString *symbolFrom = currencyInfoFrom[@"symbol"];
    CGFloat valueTo = [self getValueToForCurrencyFrom:currencyFrom
                                           currencyTo:currencyTo
                                        currencyRates:currencyRates];
    NSString *symbolTo = currencyInfoTo[@"symbol"];
    CGFloat reverseValue = [self getValueToForCurrencyFrom:currencyTo
                                                currencyTo:currencyFrom
                                             currencyRates:currencyRates];
    NSDictionary *pairInfo = @{@"currencyFrom":currencyFrom,
                               @"valueFrom":[NSNumber numberWithInteger:valueFrom],
                               @"symbolFrom":symbolFrom,
                               @"currencyTo":currencyTo,
                               @"valueTo":[NSNumber numberWithFloat:valueTo],
                               @"symbolTo":symbolTo,
                               @"reverseValue":[NSNumber numberWithFloat:reverseValue]};
    return pairInfo;
}


- (CGFloat) getValueToForCurrencyFrom:(NSString*)currencyFrom
                           currencyTo:(NSString*)currencyTo
                        currencyRates:(NSArray*)currencyRates {
    if ([currencyFrom isEqualToString:@"EUR"]) {
        return [self valueForCurrency:currencyTo
                        currencyRates:currencyRates];
    }
    CGFloat rateFrom = [self valueForCurrency:currencyFrom currencyRates:currencyRates];
    CGFloat rateTo = [self valueForCurrency:currencyTo currencyRates:currencyRates];
    return rateTo/rateFrom;
}
                
- (CGFloat) valueForCurrency:(NSString*)currency
               currencyRates:(NSArray*)currencyRates {
    // if EUR return 1.0;
    CGFloat valueTo = 1.0f;
    for (TVCurrencyModel *model in currencyRates) {
        if ([model.currency isEqualToString:currency]) {
            valueTo = model.rate;
        }
    }
    return valueTo;
}

- (void) setCurrencyRateWithInfo:(NSDictionary*)currencyInfo {
    _currencyFrom = currencyInfo[@"currencyFrom"];
    _symbolFrom = currencyInfo[@"symbolFrom"];
    _rateValueFrom = [currencyInfo[@"valueFrom"] integerValue];
    _currencyTo = currencyInfo[@"currencyTo"];
    _symbolTo = currencyInfo[@"symbolTo"];
    _rateValueTo = [currencyInfo[@"valueTo"] floatValue];
    _reverseValue =  [currencyInfo[@"reverseValue"] floatValue];
}

- (void) updateCurrencyRateFrom:(NSString*)curenncyFrom  {
    for (NSDictionary *currencyPair in _currencyRateVariants) {
        if ([currencyPair[@"currencyFrom"] isEqualToString:curenncyFrom] &&
            [_currencyTo isEqualToString:currencyPair[@"currencyTo"]]) {
            [self setCurrencyRateWithInfo:currencyPair];
        } else if ([currencyPair[@"currencyFrom"] isEqualToString:curenncyFrom] &&
                   [_currencyTo isEqualToString:currencyPair[@"currencyFrom"]]) {
            NSDictionary *info = @{@"currencyFrom":currencyPair[@"currencyFrom"],
                                   @"symbolFrom":currencyPair[@"symbolFrom"],
                                   @"valueFrom":currencyPair[@"valueFrom"],
                                   @"currencyTo":currencyPair[@"currencyFrom"],
                                   @"symbolTo":currencyPair[@"symbolFrom"],
                                   @"valueTo":currencyPair[@"valueFrom"],
                                   @"reverseValue":[NSNumber numberWithFloat:1.0f]
                                   };
            [self setCurrencyRateWithInfo:info];
        }
    }
}

- (void) updateCurrencyRateTo:(NSString*)currencyTo  {
    for (NSDictionary *currencyPair in _currencyRateVariants) {
        if ([currencyPair[@"currencyTo"] isEqualToString:currencyTo] &&
            [_currencyFrom isEqualToString:currencyPair[@"currencyFrom"]]) {
            [self setCurrencyRateWithInfo:currencyPair];
        }  else if ([currencyPair[@"currencyTo"] isEqualToString:currencyTo] &&
                    [_currencyFrom isEqualToString:currencyPair[@"currencyTo"]]) {
            NSDictionary *info = @{@"currencyFrom":currencyPair[@"currencyTo"],
                                   @"symbolFrom":currencyPair[@"symbolTo"],
                                   @"valueFrom":currencyPair[@"valueFrom"],
                                   @"currencyTo":currencyPair[@"currencyTo"],
                                   @"symbolTo":currencyPair[@"symbolTo"],
                                   @"valueTo":currencyPair[@"valueFrom"],
                                   @"reverseValue":[NSNumber numberWithFloat:1.0f]
                                   };
            [self setCurrencyRateWithInfo:info];
        }
    }
}

- (void) updateCurrencyWithPair:(NSDictionary*)currencyPair {
    _currencyFrom = currencyPair[@"currencyFrom"];
    _symbolFrom = currencyPair[@"symbolFrom"];
    _rateValueFrom = [currencyPair[@"valueFrom"] integerValue];
    _currencyTo = currencyPair[@"currencyTo"];
    _symbolTo = currencyPair[@"symbolTo"];
    _rateValueTo = [currencyPair[@"valueTo"] floatValue];
    _reverseValue = [currencyPair[@"reverseValue"] floatValue];
}

- (NSDictionary*) currencyPairInfo {
    return   @{@"currencyFrom":_currencyFrom,
               @"valueFrom":[NSNumber numberWithInteger:_rateValueFrom],
               @"symbolFrom":_symbolFrom,
               @"currencyTo":_currencyTo,
               @"valueTo":[NSNumber numberWithFloat:_rateValueTo],
               @"symbolTo":_symbolTo,
               @"reverseValue":[NSNumber numberWithFloat:_reverseValue]
             };
}

- (CGFloat) calculateValueToWithValueFrom:(CGFloat)userValueFrom {
    CGFloat userValueTo = userValueFrom * self.rateValueTo;
    return userValueTo;
}

- (CGFloat) calculateValueFromWithValueTo:(CGFloat)userValueTo {
    CGFloat userValueFrom = userValueTo * self.reverseValue;
    return userValueFrom;

}

- (void) updateBalanceForPage:(NSInteger)page
                    withValue:(CGFloat)value {
    NSString *currency = [_userBalanceInfo allKeys][page];
    NSMutableDictionary *currencyInfo = [NSMutableDictionary dictionaryWithDictionary:_userBalanceInfo[currency]];
    CGFloat newBalance = [currencyInfo[@"balance"] floatValue] + value;
    CGFloat roundBalance = roundf (newBalance * 100) / 100.0;
    currencyInfo[@"balance"] = [NSNumber numberWithFloat:roundBalance];
    _userBalanceInfo[currency] = currencyInfo;
    [self saveUserBalance];
}

- (BOOL) hasExchangeOperationCurrentCurrencyPair {
    NSDictionary *currencyInfo = _userBalanceInfo[self.currencyFrom];
    if (currencyInfo[@"operations"]== nil) {
        return NO;
    } else {
        NSArray *arrOperations = currencyInfo[@"operations"];
        for (NSDictionary *operation in arrOperations) {
            if ([operation[@"exchangeCurrencyTo"] isEqualToString:self.currencyTo]) {
                return YES;
            }
        }
    }
    return NO;
}

- (void) saveExchangeOperationWithDeductValue:(CGFloat)value {
    NSMutableDictionary *currencyInfo = [NSMutableDictionary dictionaryWithDictionary:_userBalanceInfo[self.currencyFrom]];
    NSMutableArray *arrOperations = currencyInfo[@"operations"];
    if (!arrOperations) {
        arrOperations = [NSMutableArray array];
    }
    NSDictionary *lastOperationInfo = @{@"exchangeCurrencyTo":self.currencyTo,
                                        @"exchangeValue":[NSNumber numberWithFloat: value]};
    [arrOperations addObject:lastOperationInfo];
    currencyInfo[@"operations"] = arrOperations;
    _userBalanceInfo[self.currencyFrom] = currencyInfo;
    [self saveUserBalance];
}

- (void) cancelLastExchangeOperation {
    NSMutableDictionary *currencyInfoFrom = [NSMutableDictionary dictionaryWithDictionary:_userBalanceInfo[self.currencyFrom]];
    NSMutableArray *arrOperations = currencyInfoFrom[@"operations"];
    NSDictionary *lastOperationInfo = [self getLastOperationFromOperations:arrOperations];
    CGFloat balanceFrom = [currencyInfoFrom[@"balance"] floatValue] + [lastOperationInfo[@"exchangeValue"] floatValue];
    CGFloat roundBalanceFrom = roundf (balanceFrom * 100) / 100.0;
    currencyInfoFrom[@"balance"] = [NSNumber numberWithFloat:roundBalanceFrom];
    
    NSMutableDictionary *currencyInfoTo = [NSMutableDictionary dictionaryWithDictionary:_userBalanceInfo[lastOperationInfo[@"exchangeCurrencyTo"]]];
    CGFloat balanceTo = [currencyInfoTo[@"balance"] floatValue] - [lastOperationInfo[@"exchangeValue"] floatValue] * self.rateValueTo; // return value that deducted;
    CGFloat roundBalanceTo = roundf (balanceTo * 100) / 100.0;
    currencyInfoTo[@"balance"] = [NSNumber numberWithFloat:roundBalanceTo];
    [arrOperations removeObject:lastOperationInfo];
    _userBalanceInfo[self.currencyFrom] = currencyInfoFrom;
    _userBalanceInfo[self.currencyTo] = currencyInfoTo;
    [self saveUserBalance];
}

- (NSDictionary*) getLastOperationFromOperations:(NSArray*)arrOperations {
    NSDictionary *lastOperationInfo = nil;
    for (NSDictionary *operation in arrOperations) {
        if([operation[@"exchangeCurrencyTo"] isEqualToString:self.currencyTo]) {
            lastOperationInfo = operation;
        }
    }
    return lastOperationInfo;
}

#pragma mark - write Dictionary to plist file
- (void) saveUserBalance {
    NSString *data = [NSString stringWithFormat:@"%@", _userBalanceInfo];
    [data writeToFile:_pathToUserBalance atomically:NO encoding:NSUTF8StringEncoding error:nil];
}



@end
