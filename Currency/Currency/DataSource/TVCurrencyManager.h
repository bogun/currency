//
//  TVCurrencyManager.h
//  Currency
//
//  Created by Valentin Titov on 08.07.16.
//  Copyright © 2016 Valentin Titov. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TVCurrencyManager : NSObject

@property(readonly, nonatomic) NSString *currencyFrom;
@property(readonly, nonatomic) NSString *symbolFrom;
@property(readonly, nonatomic) NSInteger rateValueFrom;
@property(readonly, nonatomic) NSString *currencyTo;
@property(readonly, nonatomic) NSString *symbolTo;
@property(readonly, nonatomic) CGFloat rateValueTo;
@property(readonly, nonatomic) CGFloat reverseValue;

/** Singleton */
+ (instancetype) currencyManager;
/** Get list of user currency 
 *  @return An array with currency names
 */
- (NSArray*) userCurrency;
/** Get list of user balance for each currency
 *  @return A dictionary with info for each currency
 */
- (NSDictionary*) userBalanceInfo;
/** Get info only for current displayed currency pair
 *  @return A dictionary with info for each displayed currency
 */
- (NSDictionary*) currencyPairInfo;
/** Get count of all user currency
 *  @return An integer with currency count
 */
- (NSInteger) count;
/** Prepare array with full info of currency for each currency rate loaded from internet
 *  @param currencyRates An array with currency rates loaded from internet
 */
- (NSArray*) fullInfoForCurrencyRates:(NSArray*) currencyRates;
/** Set currency rates info for current currency pair
 *  @param currencyInfo A dictionary with rates info for current currency pair
 */
- (void) setCurrencyRateWithInfo:(NSDictionary*)currencyInfo;
/** Update info for currencyFrom
 *  @param curenncyFrom A name of currency-from which need update
 */
- (void) updateCurrencyRateFrom:(NSString*)curenncyFrom;
/** Update info for currencyTo
 *  @param curenncyTo A name of currency-to which need update
 */
- (void) updateCurrencyRateTo:(NSString*)curenncyTo;
/** Calculate value-to, when changed value-from
 *  @param userValueFrom A value set user for exchange
 */
- (CGFloat) calculateValueToWithValueFrom:(CGFloat)userValueFrom;
/** Calculate value-from, when changed value-to
 *  @param userValueTo A value set user for exchange
 */
- (CGFloat) calculateValueFromWithValueTo:(CGFloat)userValueTo;
/** Update balance for page after exchange operation
 *  @param page An integer with number of page
 *  @param value An integer value wich will deduct or added (depends operation was "from" or "to") to current currency balance
 */
- (void) updateBalanceForPage:(NSInteger)page
                    withValue:(CGFloat)value;
/** Save result of exchange operation to history
 *  @param value A value wich deduct from current currency
 */
- (void) saveExchangeOperationWithDeductValue:(CGFloat)value;
/** Check have operation in history for current currency pair
 *  @return BOOL value (yes or no)
 */
- (BOOL) hasExchangeOperationCurrentCurrencyPair;
/** Cancel from last exchange operation for current currency pair
 */
- (void) cancelLastExchangeOperation;

@end
