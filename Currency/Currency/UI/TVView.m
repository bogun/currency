//
//  TVView.m
//  Currency
//
//  Created by Valentin Titov on 08.07.16.
//  Copyright © 2016 Valentin Titov. All rights reserved.
//

#import "TVView.h"

@implementation TVView

- (void) resetConstraintForView:(UIView*)view {
    view.translatesAutoresizingMaskIntoConstraints = NO;
    [view removeConstraints:[view constraints]];
}

@end
