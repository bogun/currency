//
//  TVRateField.h
//  Currency
//
//  Created by Valentin Titov on 13.07.16.
//  Copyright © 2016 Valentin Titov. All rights reserved.
//

#import <UIKit/UIKit.h>
@class TVRateButton;

@interface TVRateField : UITextField

@property(strong, nonatomic) TVRateButton *showRates;


- (void) setTextWithCurrencyRateInfo:(NSDictionary*)currencyInfo;

@end
