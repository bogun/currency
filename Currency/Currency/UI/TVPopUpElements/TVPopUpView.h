//
//  TVPopUpView.h
//  Currency
//
//  Created by Valentin Titov on 13.07.16.
//  Copyright © 2016 Valentin Titov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TVPopUpView : UITableView

@property (assign,nonatomic) BOOL isShown;

@end
