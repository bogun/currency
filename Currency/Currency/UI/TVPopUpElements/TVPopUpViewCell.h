//
//  TVPopUpViewCell.h
//  Currency
//
//  Created by Valentin Titov on 16.07.16.
//  Copyright © 2016 Valentin Titov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TVPopUpViewCell : UITableViewCell

- (void) setTextWithInfo:(NSDictionary*)info;

@end
