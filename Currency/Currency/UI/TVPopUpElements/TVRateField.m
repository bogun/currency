//
//  TVRateField.m
//  Currency
//
//  Created by Valentin Titov on 13.07.16.
//  Copyright © 2016 Valentin Titov. All rights reserved.
//

#import "TVRateField.h"
#import "TVRateButton.h"
#import "TVUIConst.h"
#import "NSArray+AutoLayout.h"
#import "NSLayoutConstraint+AutoLayout.h"
#import "UIView+AutoLayout.h"

@interface TVRateField () <UITextFieldDelegate>

@end

@implementation TVRateField

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self setStyle];
        [self addShowButton];
        [self setShowRatesButtonPosition];
        self.delegate = self;
    }
    return self;
}

#pragma mark - set rate label style

- (void) setStyle {
    self.backgroundColor = kDarkBackgroundColor;
    [self addPadding];
    [self addBorder];
    [self addFontStyle];
}

- (void) addPadding {
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, kDefaultPadding, kDefaultHeight)];
    self.leftView = paddingView;
    self.leftViewMode = UITextFieldViewModeAlways;
}

- (void) addBorder {
    self.layer.borderWidth = kDefaultBorderWidth;
    self.layer.cornerRadius = kDefaultBorderCornerRadius;
    self.layer.borderColor = kBackgroundMinColor.CGColor;
}

- (void) addFontStyle {
    [self setFont:[UIFont fontWithName:kDefaultFontFamily size:kDefaultFontSize]];
    [self setTextColor:kDefaultFontColor];
}

#pragma mark - show elements
- (void) addShowButton {
    self.showRates = [[TVRateButton alloc] init];
    [self addSubview:self.showRates];
}


#pragma mark - set elements position
- (void) setShowRatesButtonPosition {
    [self.showRates setFrame:CGRectMake(0.0f, 0.0f, kDefaultHeight, kDefaultHeight)];
    self.rightView = self.showRates;
    self.rightViewMode =  UITextFieldViewModeAlways;
}

#pragma mark - work with text
- (void) setTextWithCurrencyRateInfo:(NSDictionary*)currencyInfo {
    NSString *symbolFrom = currencyInfo[@"symbolFrom"];
    NSInteger valueFrom = [currencyInfo[@"valueFrom"] integerValue];
    NSString *symbolTo = currencyInfo[@"symbolTo"];
    CGFloat valueTo = [currencyInfo[@"valueTo"] floatValue];
    [self setText:[NSString stringWithFormat:@"%@%ld = %@%0.4f",symbolFrom,(long)valueFrom,symbolTo,valueTo]];
}

#pragma mark - UITextFieldDelegate
-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    return NO;
}

@end
