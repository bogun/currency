//
//  TVPopUpView.m
//  Currency
//
//  Created by Valentin Titov on 13.07.16.
//  Copyright © 2016 Valentin Titov. All rights reserved.
//

#import "TVPopUpView.h"
#import "TVUIConst.h"

@implementation TVPopUpView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setStyle];
    }
    return self;
}

- (void) setStyle {
    self.backgroundColor = [UIColor clearColor];
    [self.layer setCornerRadius:kDefaultBorderCornerRadius];
    [self.layer setBorderWidth:kDefaultBorderWidth];
    [self.layer setBorderColor:kBackgroundMinColor.CGColor];
    
}

@end
