//
//  TVRateButton.m
//  Currency
//
//  Created by Valentin Titov on 13.07.16.
//  Copyright © 2016 Valentin Titov. All rights reserved.
//

#import "TVRateButton.h"
#import "TVUIConst.h"
@implementation TVRateButton

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self setStyle];
    }
    return self;
}

- (void) setStyle {
    [self setTitle:@"▼" forState:UIControlStateNormal];
    [self.titleLabel setFont:[UIFont fontWithName:kDefaultFontFamily size:kDefaultFontSize]];
    [self setTitleColor:kDefaultFontColor forState:UIControlStateNormal];
}

@end
