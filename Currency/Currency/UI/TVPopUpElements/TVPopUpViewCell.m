//
//  TVPopUpViewCell.m
//  Currency
//
//  Created by Valentin Titov on 16.07.16.
//  Copyright © 2016 Valentin Titov. All rights reserved.
//

#import "TVPopUpViewCell.h"
#import "TVUIConst.h"

@implementation TVPopUpViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(nullable NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setStyle];
    }
    return self;
}

- (void) setStyle {
    self.backgroundColor = [UIColor colorWithWhite:0.0f alpha:0.7f];
    [self.textLabel setFont:[UIFont fontWithName:kDefaultFontFamily size:kDefaultFontSize]];
    [self.textLabel setTextColor:kDefaultFontColor];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}


- (void) setTextWithInfo:(NSDictionary*)info {
    NSString *symbolFrom = info[@"symbolFrom"];
    NSInteger valueFrom = [info[@"valueFrom"] integerValue];
    NSString *symbolTo = info[@"symbolTo"];
    CGFloat valueTo = [info[@"valueTo"] floatValue];
    [self.textLabel setText:[NSString stringWithFormat:@"%@%ld = %@%0.4f",symbolFrom,(long)valueFrom,symbolTo,valueTo]];
}

@end
