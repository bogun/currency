//
//  TVRound.h
//  Currency
//
//  Created by Valentin on 18.07.16.
//  Copyright © 2016 Valentin Titov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TVRound : UIView
- (instancetype)initRoundForFrame:(CGRect)frame;

@end
