//
//  TVRound.m
//  Currency
//
//  Created by Valentin on 18.07.16.
//  Copyright © 2016 Valentin Titov. All rights reserved.
//

#import "TVRound.h"
#import "TVUIConst.h"
@implementation TVRound

- (instancetype)initRoundForFrame:(CGRect)frame {
    self = [super init];
    if (self) {
        CGPoint position = [self getRandomPositionFromFrame:frame];
        CGSize size = [self getRandomSize];
        [self setFrame:CGRectMake(position.x, position.y, size.width, size.height)];
        self.layer.cornerRadius = size.width/2;
        [self setBackgroundColor:[self getRandomColor]];
    }
    return self;
}

- (CGPoint) getRandomPositionFromFrame:(CGRect)frame {
    CGFloat pointX = [self randomFloatWithMin:frame.origin.x max:frame.size.width];
    CGFloat pointY = [self randomFloatWithMin:frame.origin.y max:frame.size.height];
    CGPoint roundPosition = CGPointMake(pointX, pointY);
    return roundPosition;
}

- (CGSize) getRandomSize {
    CGFloat value = [self randomFloatWithMin:kMinRoundSide max:kMaxRoundSide];
    CGSize size = CGSizeMake(value, value);
    return size;
}

- (UIColor*) getRandomColor {
    CGFloat alpha = [self randomFloatWithMin:0.1f max:1.0f];
    UIColor *color = [UIColor colorWithRed:52.0f/255.0f green:137.0f/255.0f blue:219.0f/255.0f alpha:alpha];
    return color;
}

- (CGFloat) randomFloatWithMin:(CGFloat)min
                           max:(CGFloat)max {
  CGFloat value = ((arc4random()%RAND_MAX)/(RAND_MAX*1.0))*(max-min)+min;
    return value;
}

@end
