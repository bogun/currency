//
//  TVBarButtonItem.m
//  Currency
//
//  Created by Valentin Titov on 13.07.16.
//  Copyright © 2016 Valentin Titov. All rights reserved.
//

#import "TVBarButtonItem.h"

@implementation TVBarButtonItem

-(instancetype)initWithTitle:(NSString *)title style:(UIBarButtonItemStyle)style target:(id)target action:(SEL)action {
    self = [super initWithTitle:title style:style target:target action:action];
    if (self) {
        [self setStyle];
    }
    return self;
}

- (void) setStyle {
    self.tintColor = [UIColor whiteColor];
}

@end
