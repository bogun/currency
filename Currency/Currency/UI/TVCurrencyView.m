//
//  TVCurrencyView.m
//  Currency
//
//  Created by Valentin Titov on 06.07.16.
//  Copyright © 2016 Valentin Titov. All rights reserved.
//

#import "TVCurrencyView.h"
#import "TVCurrencyCell.h"
#import "TVPageControl.h"
#import "TVRound.h"
#import "TVUIConst.h"
#import "NSArray+AutoLayout.h"
#import "NSLayoutConstraint+AutoLayout.h"
#import "UIView+AutoLayout.h"



@interface TVCurrencyView () <TVCurrencyCellDelegate> {
    NSMutableArray *_cellStore;
    TVCurrencyCell *_lastCell;
    NSInteger _rowsCount;
}

@end

@implementation TVCurrencyView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self initCellsStore];
        [self setStyle];
    }
    return self;
}

- (void) initCellsStore {
    _cellStore = [NSMutableArray array];
}

#pragma mark - set style
- (void) setStyle {
    [self setGradient];
    [self addBackgroundRounds];
}

- (void) setGradient {
    CAGradientLayer *gradient = [CAGradientLayer layer];
    gradient.frame = self.bounds;
    gradient.colors = [NSArray arrayWithObjects:(id)kBackgroundMaxColor.CGColor, (id)kBackgroundMinColor.CGColor, nil];
    [self.layer insertSublayer:gradient atIndex:0];
}

- (void) addBackgroundRounds {
    for (NSInteger index = 0; index < roundCount; index++) {
        TVRound *round = [[TVRound alloc] initRoundForFrame:self.frame];
        [self addSubview:round];
    }
}


#pragma mark - show elements
- (void)setDataSource:(id<TVCurrencyViewDataSource>)dataSource {
    _dataSource = dataSource;
    [self reloadData];
}

- (void) reloadData {
    [self showElements];
}

- (void) resetCellStore {
    [_cellStore removeAllObjects];
}

- (void) showElements {
    NSInteger sectionCount = [_dataSource numbersOfSectionsInView:(self)];
    if ([self checkSectionCount:sectionCount]) {
        [self showSectionsWithCount:sectionCount];
        
    } else {
        NSLog(@"Sorry, this version API not support count of section not equal to 2. Please, set number '2' in method numbersOfSectionsInView: ");
    };
}

- (BOOL) checkSectionCount:(NSInteger) count {
    if (count != 2) {
        return NO;
    }
    return YES;
}

- (void) showSectionsWithCount:(NSInteger) count {
    for (NSInteger section = 0; section < count; section++) {
        TVIndex *index = [[TVIndex alloc] init];
        index.section = section;
        index.row = 0;
        _rowsCount = [_dataSource currencyView:self numberOfRowsInSection:section];
        [self showSectionAtIndex:index];
    }
}

- (void) showSectionAtIndex:(TVIndex*)index {
    TVCurrencyCell *currencyCell = [self.dataSource currencyView:self cellForRowAtIndex:index];
    [self addSubview:currencyCell];
    currencyCell.section = index.section;
    currencyCell.delegate = self;
    [currencyCell setNumberOfPages:_rowsCount];
    if (index.section == 0) {
        [self setPositionForFirstCell:currencyCell];
        [currencyCell setUserValueSign:@"-"];
    } else {
        [self setPositionForOtherCell:currencyCell];
        [currencyCell setCurrentPage:1];
        [currencyCell setUserValueSign:@"+"];
    }
    [_cellStore addObject:currencyCell];
}


#pragma mark - Set positions
- (void) updateElementsPosition {
    
    for (TVCurrencyCell *cell in _cellStore) {
        if (cell.section == 0) {
            [self setPositionForFirstCell:cell];
        } else {
            [self setPositionForOtherCell:cell];
        }
    }
}

- (void) setPositionForFirstCell:(TVCurrencyCell*)cell {
    [self resetConstraintForView:cell];
    [[cell ad_pinEdgesToSameEdgesOfSuperView:UIRectEdgeTop|UIRectEdgeLeft|UIRectEdgeRight]ad_installConstraints];
    [[cell ad_height:self.center.y] ad_install];
    [cell setElementsPosition];
    _lastCell = cell;
}

- (void) setPositionForOtherCell:(TVCurrencyCell*)cell {
    [self resetConstraintForView:cell];
    [[cell ad_pinEdgesToSameEdgesOfSuperView:UIRectEdgeLeft|UIRectEdgeBottom|UIRectEdgeRight]ad_installConstraints];
    [[cell ad_pinEdge:UIRectEdgeTop toEdge:UIRectEdgeBottom ofView:_lastCell] ad_install];
    [cell setElementsPosition];
    _lastCell = cell;
}



#pragma mark - TVCurrencyCellDelegate
- (void)currencyCell:(TVCurrencyCell *)cell
    displayPageByNumber:(NSInteger)number {
    
    for (TVCurrencyCell *cellInStore in _cellStore) {
        if ([cell isEqual:cellInStore]) {
            if (number < 0) {
                number = _rowsCount-1;
                cellInStore.currentPage = number;
            } else if (number > _rowsCount-1) {
                number = 0;
                cellInStore.currentPage = number;
            }
        }
        TVIndex *index = [[TVIndex alloc] initWithSection:cellInStore.section row:cellInStore.currentPage];
        [self.dataSource currencyCell:cellInStore displayCellAtIndex:index];
    }
}

- (void) setCurrencyPageFrom:(NSInteger)pageFrom
              currencyPageTo:(NSInteger)pageTo {
    for (TVCurrencyCell *cell in _cellStore) {
        TVIndex *index = nil;
        if (cell.section == 0) {
            index = [[TVIndex alloc] initWithSection:cell.section row:pageFrom];
        } else if (cell.section == 1) {
            index = [[TVIndex alloc] initWithSection:cell.section row:pageTo];
        }
        [self.dataSource currencyCell:cell displayCellAtIndex:index];
        [cell setCurrentPage:index.row];
    }
}

- (NSInteger) currentPageCurrencyFrom {
    NSInteger currentPage = 0;
    for (TVCurrencyCell *cell in _cellStore) {
        if (cell.section == 0) {
            currentPage = cell.currentPage;
        }
    }
    return currentPage;
}

- (NSInteger) currentPageCurrencyTo {
    NSInteger currentPage = 0;
    for (TVCurrencyCell *cell in _cellStore) {
        if (cell.section == 1) {
            currentPage = cell.currentPage;
        }
    }
    return currentPage;
}

@end


@implementation TVIndex

- (instancetype)initWithSection:(NSInteger)section
                            row:(NSInteger)row {
    self = [super init];
    if (self) {
        _section = section;
        _row = row;
    }
    return self;
}

@end

