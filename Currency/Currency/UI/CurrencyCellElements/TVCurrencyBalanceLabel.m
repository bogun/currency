//
//  TVCurrencyBalanceLabel.m
//  Currency
//
//  Created by Valentin Titov on 08.07.16.
//  Copyright © 2016 Valentin Titov. All rights reserved.
//

#import "TVCurrencyBalanceLabel.h"
#import "TVUIConst.h"
@implementation TVCurrencyBalanceLabel

- (instancetype)init {
    self = [super init];
    if (self) {
        [self setStyle];
    }
    return self;
}

- (void) setStyle {
    [self setFont:[UIFont fontWithName:kDefaultFontFamily size:kDefaultFontSize]];
    [self setTextAlignment:NSTextAlignmentCenter];
    [self setDefaultColor];
}

- (void) setDefaultColor {
    [self setTextColor:kDefaultBalanceColor];
}

- (void) setWarningColor {
    [self setTextColor:kWarningBalanceColor];
}


- (void) setValue:(CGFloat)value
           symbol:(NSString*)symbol {
    [self setText:[NSString stringWithFormat:@"You have %@%0.2f",symbol,value]];
}



@end
