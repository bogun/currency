//
//  TVCurrencyCell.h
//  Currency
//
//  Created by Valentin Titov on 08.07.16.
//  Copyright © 2016 Valentin Titov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TVView.h"

@class
TVCurrencyCell,
TVCurrencyLabel,
TVCurrencyField,
TVCurrencyBalanceLabel,
TVCurrencyRateReverseLabel,
TVPageControl;

@protocol TVCurrencyCellDelegate <NSObject>
/** Call when user swiped on cell
 *  @param cell A cell which swiped
 *  @param number A new number for display cell
 */
- (void)currencyCell:(TVCurrencyCell *)cell
 displayPageByNumber:(NSInteger)number;

@end


@interface TVCurrencyCell : TVView
/* Name of currency */
@property (strong, nonatomic) TVCurrencyLabel *currencyTitle;
/* User value for current currency. By default is 0 */
@property (strong, nonatomic) TVCurrencyField *currencyValue;
/* User balance for current currency */
@property (strong, nonatomic) TVCurrencyBalanceLabel *balance;
/* Reverse currency rate for current currency pair. Display only for currency-to */
@property (strong, nonatomic) TVCurrencyRateReverseLabel *currencyRateReverse;
/* Current cell section */
@property (assign, nonatomic) NSInteger section;
/* Current cell page */
@property (assign, nonatomic) NSInteger currentPage;
/* Delegate of TVCurrencyCellDelegate protocol */
@property (strong, nonatomic) id <TVCurrencyCellDelegate> delegate;
/** Will set dark background only for cell currency-to
 */
- (void) setDarkBackground;
/** Set position for all cell elements
 */
- (void) setElementsPosition;
/** Set cell info with special format
 *  @param title A string with name of currency
 *  @param balance A float value with user balance
 *  @param symbol A string with currency symbol
 */
- (void) setCurrencyTitle:(NSString*)title
                  balance:(CGFloat)balance
                   symbol:(NSString*) symbol;
/** Set cell rate reverse info with special format (only for currency-to)
 *  @param valueFrom A float value for currency from
 *  @param symbolFrom A string with currency symbol for currency from
 *  @param valueTo A float value for currency to
 *  @param symbolTo A string with currency symbol for currency to
 */
- (void) setRateValueFrom:(CGFloat)valueFrom
               symbolFrom:(NSString*)symbolFrom
              rateValueTo:(CGFloat)valueTo
                 symbolTo:(NSString*)symbolTo;
/** Set balance label style when balance value less then minimum value
 */
- (void) setWrongBalanceStyle;
/** Set default balance label style
 */
- (void) setDefaultBalanceStyle;
/** Set number of pages for pages control
 *  @param number A number of pages;
 */
- (void) setNumberOfPages:(NSInteger)number;
/** Set sign for currency-from and currency-to
 *  @param number A string with sign "+" or "-"
 */
- (void) setUserValueSign:(NSString*)sign;

@end
