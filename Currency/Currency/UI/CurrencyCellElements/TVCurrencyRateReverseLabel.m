//
//  TVCurrencyRateReverseLabel.m
//  Currency
//
//  Created by Valentin Titov on 08.07.16.
//  Copyright © 2016 Valentin Titov. All rights reserved.
//

#import "TVCurrencyRateReverseLabel.h"
#import "TVUIConst.h"

@implementation TVCurrencyRateReverseLabel

- (instancetype)init {
    self = [super init];
    if (self) {
        [self setStyle];
    }
    return self;
}

- (void) setStyle {
    [self setFont:[UIFont fontWithName:kDefaultFontFamily size:kDefaultFontSize]];
    [self setTextAlignment:NSTextAlignmentCenter];
    [self setTextColor:kDefaultBalanceColor];
}

- (void) setValueFrom:(CGFloat)valueFrom
           symbolFrom:(NSString*)symbolFrom
              valueTo:(CGFloat)valueTo
             symbolTo:(NSString*)symbolTo {
    [self setText:[NSString stringWithFormat:@"%@%0.2f = %@%0.2f",symbolFrom,valueFrom,symbolTo,valueTo]];
}


@end
