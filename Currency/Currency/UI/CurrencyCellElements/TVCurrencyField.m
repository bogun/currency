//
//  TVCurrencyField.m
//  Currency
//
//  Created by Valentin Titov on 08.07.16.
//  Copyright © 2016 Valentin Titov. All rights reserved.
//

#import "TVCurrencyField.h"
#import "TVUIConst.h"

@interface TVCurrencyField() {
    UILabel *_signLabel;
}

@end

@implementation TVCurrencyField

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self setStyle];
    }
    return self;
}

- (void) setStyle {
    [self setFont:[UIFont fontWithName:kDefaultFontFamily size:kBigFontSize]];
    [self setTextColor:kDefaultFontColor];
    [self setTextAlignment:NSTextAlignmentLeft];
    [self setKeyboardType:UIKeyboardTypeDecimalPad];
    [self setText:@"0.00"];
    
    _signLabel = [[UILabel alloc] initWithFrame:CGRectMake(0.0f, 0.0f, kDefaultPadding*2, kDefaultHeight)];
    [_signLabel setFont:[UIFont fontWithName:kDefaultFontFamily size:kBigFontSize]];
    [_signLabel setTextColor:kDefaultFontColor];
    _signLabel.backgroundColor = [UIColor colorWithWhite:0.0 alpha:0.0];
    self.leftViewMode = UITextFieldViewModeAlways;
    self.leftView = _signLabel;
}

- (void) setUserValueSign:(NSString*)sign {
    _signLabel.text = sign;
}


@end
