//
//  TVCurrencyBalanceLabel.h
//  Currency
//
//  Created by Valentin Titov on 08.07.16.
//  Copyright © 2016 Valentin Titov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TVCurrencyBalanceLabel : UILabel

- (void) setValue:(CGFloat)value
           symbol:(NSString*)symbol;

- (void) setDefaultColor;
- (void) setWarningColor;

@end
