//
//  TVCurrencyCell.m
//  Currency
//
//  Created by Valentin Titov on 08.07.16.
//  Copyright © 2016 Valentin Titov. All rights reserved.
//

#import "TVCurrencyCell.h"
#import "TVCurrencyLabel.h"
#import "TVCurrencyField.h"
#import "TVCurrencyBalanceLabel.h"
#import "TVPageControl.h"
#import "TVCurrencyRateReverseLabel.h"
#import "NSArray+AutoLayout.h"
#import "NSLayoutConstraint+AutoLayout.h"
#import "UIView+AutoLayout.h"
#import "TVUIConst.h"

@interface TVCurrencyCell () {
    TVPageControl *_pageControl;
}
@end

@implementation TVCurrencyCell

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self addGestureRecognizers];
        [self showElements];
    }
    return self;
}

- (void) setDarkBackground {
    [self setBackgroundColor:kDarkBackgroundColor];
}

#pragma mark - add Gesture recognizers

- (void) addGestureRecognizers {
    UISwipeGestureRecognizer *swipeLeft = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipedScreen:)];
    swipeLeft.direction = UISwipeGestureRecognizerDirectionLeft;
    [self addGestureRecognizer:swipeLeft];
    
    UISwipeGestureRecognizer *swipeRight = [[UISwipeGestureRecognizer alloc] initWithTarget:self  action:@selector(swipedScreen:)];
    swipeRight.direction = UISwipeGestureRecognizerDirectionRight;
    [self addGestureRecognizer:swipeRight];
    
}

- (void) swipedScreen:(UISwipeGestureRecognizer*)swipeGesture {
    if (swipeGesture.direction == UISwipeGestureRecognizerDirectionLeft) {
        self.currentPage++;
        
    } else if (swipeGesture.direction == UISwipeGestureRecognizerDirectionRight) {
        self.currentPage--;
    }
    [self.delegate currencyCell:self
            displayPageByNumber:self.currentPage];
    _pageControl.currentPage = self.currentPage;
}

#pragma mark - show elements
- (void) showElements {
    [self showCurrencyTitle];
    [self showCurrencyValue];
    [self showBalance];
    [self showPageControl];
    [self showCurrencyRateReverse];
}

- (void) showCurrencyTitle {
    self.currencyTitle = [[TVCurrencyLabel alloc] init];
    [self addSubview:self.currencyTitle];
}

- (void) showCurrencyValue {
    self.currencyValue = [[TVCurrencyField alloc] init];
    [self addSubview:self.currencyValue];
}

- (void) showBalance {
    self.balance = [[TVCurrencyBalanceLabel alloc] init];
    [self addSubview:self.balance];
}

- (void) showPageControl {
    _pageControl = [[TVPageControl alloc] init];
    [self addSubview:_pageControl];
}

- (void) setNumberOfPages:(NSInteger)number {
    _pageControl.numberOfPages = number;
}

- (void) setCurrentPage:(NSInteger)currentPage {
    _currentPage = currentPage;
    _pageControl.currentPage = currentPage;
}

- (void) showCurrencyRateReverse {
    self.currencyRateReverse = [[TVCurrencyRateReverseLabel alloc] init];
    [self addSubview:self.currencyRateReverse];
}


#pragma mark - set positions
- (void) setElementsPosition {
    [self setCurrencyTitlePosition];
    [self setCurrencyValuePosition];
    [self setBalancePosition];
    [self setPageControlPosition];
    [self setCurrencyRateReversePosition];
}

- (void) setCurrencyTitlePosition {
    [self resetConstraintForView:self.currencyTitle];
    [[self.currencyTitle ad_pinEdgesToSameEdgesOfSuperView:UIRectEdgeTop|UIRectEdgeLeft| UIRectEdgeBottom insets:UIEdgeInsetsMake(30.0f, 0.0f, 0.0f, 0.0f)] ad_installConstraints];
    [[self.currencyTitle ad_width:self.superview.center.x] ad_install];
}

- (void) setCurrencyValuePosition {
    [self resetConstraintForView:self.currencyValue];
    [[self.currencyValue ad_pinEdgeToSameEdgeOfSuperView:UIRectEdgeRight]ad_install];
    [[self.currencyValue ad_pinEdge:UIRectEdgeTop toEdge:UIRectEdgeTop ofView:self.currencyTitle]ad_install];
    [[self.currencyValue ad_pinEdge:UIRectEdgeLeft
                             toEdge:UIRectEdgeRight
                             ofView:self.currencyTitle] ad_install];
    [[self.currencyValue ad_pinEdge:UIRectEdgeBottom
                             toEdge:UIRectEdgeBottom
                             ofView:self.currencyTitle] ad_install];
}

- (void) setBalancePosition {
    [self resetConstraintForView:self.balance];
    [[self.balance ad_pinEdgesToSameEdgesOfSuperView:UIRectEdgeLeft|UIRectEdgeBottom insets:UIEdgeInsetsMake(0.0f, 0.0f, -kDefaultHeight/3, -0.0f)]ad_installConstraints];
    [[self.balance ad_pinEdge:UIRectEdgeRight
                       toEdge:UIRectEdgeRight
                       ofView:self.currencyTitle] ad_install];
    [[self.balance ad_height:kDefaultHeight] ad_install];
}

- (void) setPageControlPosition {
    [self resetConstraintForView:_pageControl];
    [[_pageControl ad_toAlignOnAxisX]ad_install];
    [[_pageControl ad_pinEdgeToSameEdgeOfSuperView:UIRectEdgeBottom] ad_install];
    [[_pageControl ad_height:kDefaultHeight]ad_install];
}

- (void) setCurrencyRateReversePosition {
    [self resetConstraintForView:self.currencyRateReverse];
    [[self.currencyRateReverse ad_pinEdgeToSameEdgeOfSuperView:UIRectEdgeRight] ad_install];
    [[self.currencyRateReverse ad_pinEdge:UIRectEdgeTop
                                   toEdge:UIRectEdgeTop
                                   ofView:self.balance] ad_install];
    [[self.currencyRateReverse ad_pinEdge:UIRectEdgeLeft
                                   toEdge:UIRectEdgeRight
                                   ofView:_pageControl] ad_install];
    [[self.currencyRateReverse ad_pinEdge:UIRectEdgeBottom
                                   toEdge:UIRectEdgeBottom
                                   ofView:self.balance] ad_install];
}


#pragma mark - set elements values
- (void) setCurrencyTitle:(NSString*)title
                  balance:(CGFloat)balance
                   symbol:(NSString*) symbol {

    [self.currencyTitle setText:title];
    [self.balance setValue:balance
                    symbol:symbol];
}


- (void) setRateValueFrom:(CGFloat)valueFrom
               symbolFrom:(NSString*)symbolFrom
              rateValueTo:(CGFloat)valueTo
                 symbolTo:(NSString*)symbolTo {
    
    [self.currencyRateReverse setValueFrom:valueFrom
                                symbolFrom:symbolFrom
                                   valueTo:valueTo
                                  symbolTo:symbolTo];
}

- (void) setUserValueSign:(NSString*)sign {
    [self.currencyValue setUserValueSign:sign];
}

#pragma mark - set elements style
- (void) setWrongBalanceStyle {
    [self.balance setWarningColor];
}

- (void) setDefaultBalanceStyle {
    [self.balance setDefaultColor];
}



@end
