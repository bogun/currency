//
//  TVCurrencyLabel.m
//  Currency
//
//  Created by Valentin Titov on 06.07.16.
//  Copyright © 2016 Valentin Titov. All rights reserved.
//

#import "TVCurrencyLabel.h"
#import "TVUIConst.h"

@implementation TVCurrencyLabel

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self setStyle];
    }
    return self;
}

- (void) setStyle {
    [self setFont:[UIFont fontWithName:kDefaultFontFamily size:kBigFontSize]];
    [self setTextColor:kDefaultFontColor];
    [self setTextAlignment:NSTextAlignmentCenter];
  }

@end
