//
//  TVPageControl.m
//  Currency
//
//  Created by Valentin Titov on 07.07.16.
//  Copyright © 2016 Valentin Titov. All rights reserved.
//

#import "TVPageControl.h"
#import "TVUIConst.h"

@implementation TVPageControl

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self setStyle];
    }
    return self;
}

- (void) setStyle {
    self.pageIndicatorTintColor = kBackgroundMinColor;
    self.currentPageIndicatorTintColor = kDefaultFontColor;
}

@end
