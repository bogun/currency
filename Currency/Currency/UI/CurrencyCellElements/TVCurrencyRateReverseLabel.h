//
//  TVCurrencyRateReverseLabel.h
//  Currency
//
//  Created by Valentin Titov on 08.07.16.
//  Copyright © 2016 Valentin Titov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TVCurrencyRateReverseLabel : UILabel

- (void) setValueFrom:(CGFloat)valueFrom
           symbolFrom:(NSString*)symbolFrom
              valueTo:(CGFloat)valueTo
             symbolTo:(NSString*)symbolTo;

@end
