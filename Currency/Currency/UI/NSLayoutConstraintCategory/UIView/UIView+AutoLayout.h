//
// Created by Anton Dobkin on 15.02.15.
// Copyright (c) 2015 Anton Dobkin. All rights reserved.
//

#import <UIKit/UIView.h>

UIKIT_STATIC_INLINE UIEdgeInsets ADEdgeInsets(CGFloat inset) {
    return (UIEdgeInsets) {inset, inset, -inset, -inset};
}

typedef NS_OPTIONS(NSInteger, ADAxis) {
    ADAxisY = 1 << 0,
    ADAxisX = 1 << 1,
};

@interface UIView (AutoLayout)

+ (instancetype)ad_viewWithAutoLayout;
- (NSLayoutConstraint *)ad_pinEdge:(UIRectEdge)edge toEdge:(UIRectEdge)toEdge ofView:(UIView *)view withInset:(CGFloat)inset relation:(NSLayoutRelation)relation;
- (NSLayoutConstraint *)ad_pinEdge:(UIRectEdge)edge toEdge:(UIRectEdge)toEdge ofView:(UIView *)view withInset:(CGFloat)inset;
- (NSLayoutConstraint *)ad_pinEdge:(UIRectEdge)edge toEdge:(UIRectEdge)toEdge ofView:(UIView *)view;

- (NSArray *)ad_pinEdgesToSameEdgesOfSuperView:(UIRectEdge)edges insets:(UIEdgeInsets)insets;
- (NSArray *)ad_pinEdgesToSameEdgesOfSuperView:(UIRectEdge)edges;

- (NSArray *)ad_pinAllEdgesToSameEdgesOfSuperView:(UIEdgeInsets)insets;
- (NSArray *)ad_pinAllEdgesToSameEdgesOfSuperView;

- (NSLayoutConstraint *)ad_pinEdgeToSameEdgeOfSuperView:(UIRectEdge)edge withInset: (CGFloat) inset relation: (NSLayoutRelation) relation;
- (NSLayoutConstraint *)ad_pinEdgeToSameEdgeOfSuperView:(UIRectEdge)edge withInset: (CGFloat) inset;
- (NSLayoutConstraint *)ad_pinEdgeToSameEdgeOfSuperView:(UIRectEdge)edge;

- (NSLayoutConstraint *)ad_toAlignOnAxis:(ADAxis)axis withInset:(CGFloat)inset;
- (NSLayoutConstraint *)ad_toAlignOnAxisYWithInset:(CGFloat)inset;
- (NSLayoutConstraint *)ad_toAlignOnAxisXWithInset:(CGFloat)inset;
- (NSLayoutConstraint *)ad_toAlignOnAxisY;
- (NSLayoutConstraint *)ad_toAlignOnAxisX;

- (NSLayoutConstraint *)ad_height:(CGFloat)height relation: (NSLayoutRelation)relation;
- (NSLayoutConstraint *)ad_height:(CGFloat)height;
- (NSLayoutConstraint *)ad_width:(CGFloat)width relation: (NSLayoutRelation)relation;
- (NSLayoutConstraint *)ad_width: (CGFloat)width;

- (NSLayoutConstraint *)ad_heightEqualToHeightOfView:(UIView *)view relation:(NSLayoutRelation)relation;
- (NSLayoutConstraint *)ad_heightEqualToHeightOfView:(UIView *)view;

- (NSLayoutConstraint *)ad_widthEqualToWidthOfView:(UIView *)view relation:(NSLayoutRelation)relation;
- (NSLayoutConstraint *)ad_widthEqualToWidthOfView:(UIView *)view;



- (UIView *)ad_commonSuperViewWithView:(UIView *)view;
@end
