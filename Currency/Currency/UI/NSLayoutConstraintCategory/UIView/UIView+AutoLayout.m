//
// Created by Anton Dobkin on 15.02.15.
// Copyright (c) 2015 Anton Dobkin. All rights reserved.
//

#import "UIView+AutoLayout.h"

@implementation UIView (AutoLayout)

+ (instancetype)ad_viewWithAutoLayout {
    UIView *view = [self new];
    view.translatesAutoresizingMaskIntoConstraints = NO;
    return view;
}

#pragma mark -

- (NSLayoutConstraint *)ad_pinEdge:(UIRectEdge)edge toEdge:(UIRectEdge)toEdge ofView:(UIView *)view withInset:(CGFloat)inset relation:(NSLayoutRelation)relation {
    NSLayoutConstraint *constraint = [NSLayoutConstraint constraintWithItem:self
                                                                  attribute:[self __ad_edgeToLayoutAttribute:edge]
                                                                  relatedBy:relation
                                                                     toItem:view
                                                                  attribute:[self __ad_edgeToLayoutAttribute:toEdge]
                                                                 multiplier:1.0f
                                                                   constant:inset];
    return constraint;
}

- (NSLayoutConstraint *)ad_pinEdge:(UIRectEdge)edge toEdge:(UIRectEdge)toEdge ofView:(UIView *)view withInset:(CGFloat)inset {
    return [self ad_pinEdge:edge toEdge:toEdge ofView:view withInset:inset relation:NSLayoutRelationEqual];
}

- (NSLayoutConstraint *)ad_pinEdge:(UIRectEdge)edge toEdge:(UIRectEdge)toEdge ofView:(UIView *)view {
    return [self ad_pinEdge:edge toEdge:toEdge ofView:view withInset:0.0f relation:NSLayoutRelationEqual];
}

- (NSArray *)ad_pinEdgesToSameEdgesOfSuperView:(UIRectEdge)edges insets:(UIEdgeInsets)insets {
    NSAssert(self.superview, @"self.superview is nil");
    NSMutableArray *constraints = [NSMutableArray array];
    NSLayoutConstraint *constraint = nil;
    if (edges & UIRectEdgeTop) {
        constraint = [self ad_pinEdge:UIRectEdgeTop toEdge:UIRectEdgeTop ofView:self.superview withInset:insets.top];
        [constraints addObject:constraint];
    }
    if (edges & UIRectEdgeLeft) {
        constraint = [self ad_pinEdge:UIRectEdgeLeft toEdge:UIRectEdgeLeft ofView:self.superview withInset:insets.left];
        [constraints addObject:constraint];
    }
    if (edges & UIRectEdgeBottom) {
        constraint = [self ad_pinEdge:UIRectEdgeBottom toEdge:UIRectEdgeBottom ofView:self.superview withInset:insets.bottom];
        [constraints addObject:constraint];
    }
    if (edges & UIRectEdgeRight) {
        constraint = [self ad_pinEdge:UIRectEdgeRight toEdge:UIRectEdgeRight ofView:self.superview withInset:insets.right];
        [constraints addObject:constraint];
    }
    return constraints;
}

- (NSArray *)ad_pinEdgesToSameEdgesOfSuperView:(UIRectEdge)edges {
    return [self ad_pinEdgesToSameEdgesOfSuperView:edges insets:UIEdgeInsetsZero];
}

- (NSArray *)ad_pinAllEdgesToSameEdgesOfSuperView:(UIEdgeInsets)insets {
    return [self ad_pinEdgesToSameEdgesOfSuperView:UIRectEdgeAll insets:insets];
}

- (NSArray *)ad_pinAllEdgesToSameEdgesOfSuperView {
    return [self ad_pinEdgesToSameEdgesOfSuperView:UIRectEdgeAll];
}

- (NSLayoutConstraint *)ad_pinEdgeToSameEdgeOfSuperView:(UIRectEdge)edge withInset: (CGFloat) inset relation: (NSLayoutRelation) relation {
    NSAssert(self.superview, @"self.superview is nil");
    return [self ad_pinEdge:edge toEdge:edge ofView:self.superview withInset:inset relation:relation];
}

- (NSLayoutConstraint *)ad_pinEdgeToSameEdgeOfSuperView:(UIRectEdge)edge withInset: (CGFloat) inset {
    return [self ad_pinEdgeToSameEdgeOfSuperView: edge withInset:inset relation:NSLayoutRelationEqual];
}

- (NSLayoutConstraint *)ad_pinEdgeToSameEdgeOfSuperView:(UIRectEdge)edge {
    return [self ad_pinEdgeToSameEdgeOfSuperView: edge withInset:0.0f];
}

#pragma mark -

- (NSLayoutConstraint *)ad_toAlignOnAxis:(ADAxis)axis withInset:(CGFloat)inset {
    NSAssert(self.superview, @"%@: superview must not be nil", self);
    NSLayoutAttribute attribute = [self __ad_axisToAttribute:axis];
    NSLayoutConstraint *constraint = [NSLayoutConstraint constraintWithItem:self
                                                                  attribute:attribute
                                                                  relatedBy:NSLayoutRelationEqual
                                                                     toItem:self.superview
                                                                  attribute:attribute
                                                                 multiplier:1.0f
                                                                   constant:inset];

    return constraint;
}

- (NSLayoutConstraint *)ad_toAlignOnAxisYWithInset:(CGFloat)inset {
    return [self ad_toAlignOnAxis:ADAxisY withInset:inset];
}

- (NSLayoutConstraint *)ad_toAlignOnAxisXWithInset:(CGFloat)inset {
    return [self ad_toAlignOnAxis:ADAxisY withInset:inset];
}

- (NSLayoutConstraint *)ad_toAlignOnAxisY {
    return [self ad_toAlignOnAxis:ADAxisY withInset:0.0f];
}

- (NSLayoutConstraint *)ad_toAlignOnAxisX {
    return [self ad_toAlignOnAxis:ADAxisX withInset:0.0f];
}

#pragma mark -

- (NSLayoutConstraint *)ad_height:(CGFloat)height relation:(NSLayoutRelation)relation {
    return [NSLayoutConstraint constraintWithItem:self
                                        attribute:NSLayoutAttributeHeight
                                        relatedBy:relation
                                           toItem:nil
                                        attribute:NSLayoutAttributeNotAnAttribute
                                       multiplier:1.0f
                                         constant:height];
}

- (NSLayoutConstraint *)ad_height:(CGFloat)height {
    return [self ad_height:height relation:NSLayoutRelationEqual];
}

- (NSLayoutConstraint *)ad_width:(CGFloat)width relation:(NSLayoutRelation)relation {
    return [NSLayoutConstraint constraintWithItem:self
                                        attribute:NSLayoutAttributeWidth
                                        relatedBy:relation
                                           toItem:nil
                                        attribute:NSLayoutAttributeNotAnAttribute
                                       multiplier:1.0f
                                         constant:width];
}

- (NSLayoutConstraint *)ad_width:(CGFloat)width {
    return [self ad_width:width relation:NSLayoutRelationEqual];
}

- (NSLayoutConstraint *)ad_heightEqualToHeightOfView:(UIView *)view relation:(NSLayoutRelation)relation {
    return [NSLayoutConstraint constraintWithItem:self
                                        attribute:NSLayoutAttributeHeight
                                        relatedBy:relation
                                           toItem:view
                                        attribute:NSLayoutAttributeHeight
                                       multiplier:1.0f
                                         constant:0.0f];
}

- (NSLayoutConstraint *)ad_heightEqualToHeightOfView:(UIView *)view {
    return [self ad_heightEqualToHeightOfView:view relation:NSLayoutRelationEqual];
}

- (NSLayoutConstraint *)ad_widthEqualToWidthOfView:(UIView *)view relation:(NSLayoutRelation)relation {
    return [NSLayoutConstraint constraintWithItem:self
                                        attribute:NSLayoutAttributeWidth
                                        relatedBy:relation
                                           toItem:view
                                        attribute:NSLayoutAttributeWidth
                                       multiplier:1.0f
                                         constant:0.0f];
}

- (NSLayoutConstraint *)ad_widthEqualToWidthOfView:(UIView *)view {
    return [self ad_widthEqualToWidthOfView:view relation:NSLayoutRelationEqual];
}

#pragma mark - Private Methods

- (UIView *)ad_commonSuperViewWithView:(UIView *)view {
    UIView *commonSuperview = nil;
    UIView *startView = self;
    do {
        if ([view isDescendantOfView:startView]) {
            commonSuperview = startView;
        }
        startView = startView.superview;
    } while (startView && !commonSuperview);
    NSAssert(commonSuperview, @"%@, %@: Does't have a common superview. Both views must be added into the same view hierarchy.", self, view);
    return commonSuperview;
}

- (NSLayoutAttribute)__ad_edgeToLayoutAttribute:(UIRectEdge)edge {
    NSLayoutAttribute attribute = NSLayoutAttributeNotAnAttribute;
    switch (edge) {
        case UIRectEdgeLeft:
            attribute = NSLayoutAttributeLeft;
            break;
        case UIRectEdgeRight:
            attribute = NSLayoutAttributeRight;
            break;
        case UIRectEdgeBottom:
            attribute = NSLayoutAttributeBottom;
            break;
        case UIRectEdgeTop:
            attribute = NSLayoutAttributeTop;
            break;
        default:
            break;
    }
    return attribute;
}

- (NSLayoutAttribute)__ad_axisToAttribute:(ADAxis)axis {
    NSLayoutAttribute attribute = NSLayoutAttributeNotAnAttribute;
    switch (axis) {
        case ADAxisX:
            attribute = NSLayoutAttributeCenterX;
            break;
        case ADAxisY:
            attribute = NSLayoutAttributeCenterY;
            break;
    }
    return attribute;
}

@end
