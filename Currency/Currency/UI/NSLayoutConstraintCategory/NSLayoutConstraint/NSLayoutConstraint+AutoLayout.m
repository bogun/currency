//
// Created by Anton Dobkin on 15.02.15.
// Copyright (c) 2015 Browsec. All rights reserved.
//

#import "NSLayoutConstraint+AutoLayout.h"
#import "UIView+AutoLayout.h"

@implementation NSLayoutConstraint (AutoLayout)

- (void)ad_installWithPriority:(UILayoutPriority)priority {
    self.priority = priority;
    if (self.firstItem && !self.secondItem) {
        [self.firstItem addConstraint:self];
    } else if (self.firstItem && self.secondItem) {
        UIView *commonSuperView = [self.firstItem ad_commonSuperViewWithView:self.secondItem];
        [commonSuperView addConstraint:self];
    }
}

- (void)ad_install {
    [self ad_installWithPriority:UILayoutPriorityRequired];
}

+ (void)ad_installConstraints:(NSArray *)constraints {
    for (id object in constraints) {
        if ([object isKindOfClass:[NSLayoutConstraint class]]) {
            [((NSLayoutConstraint *) object) ad_install];
        } else if ([object isKindOfClass:[NSArray class]]) {
            [NSLayoutConstraint ad_installConstraints:object];
        }
    }
}

- (void)ad_removeConstraint {
    if (self.secondItem) {
        UIView *commonSuperview = [self.firstItem ad_commonSuperViewWithView:self.secondItem];
        while (commonSuperview) {
            if ([commonSuperview.constraints containsObject:self]) {
                [commonSuperview removeConstraint:self];
                return;
            }
            commonSuperview = commonSuperview.superview;
        }
    } else {
        [self.firstItem removeConstraint:self];
    }
}

+ (void)ad_removeConstraints:(NSArray *)constraints {
    for (id object in constraints) {
        if ([object isKindOfClass:[NSLayoutConstraint class]]) {
            [((NSLayoutConstraint *) object) ad_removeConstraint];
        } else if ([object isKindOfClass:[NSArray class]]) {
            [NSLayoutConstraint ad_removeConstraints:object];
        }
    }
}

@end
