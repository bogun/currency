//
// Created by Anton Dobkin on 15.02.15.
// Copyright (c) 2015 Browsec. All rights reserved.
//

@import UIKit;

@interface NSLayoutConstraint (AutoLayout)
- (void)ad_installWithPriority:(UILayoutPriority)priority;
- (void)ad_install;
+ (void)ad_installConstraints:(NSArray *)constraints;
- (void)ad_removeConstraint;
+ (void)ad_removeConstraints:(NSArray *)constraints;
@end
