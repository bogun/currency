//
// Created by Anton Dobkin on 15.02.15.
// Copyright (c) 2015 Browsec. All rights reserved.
//

#import "NSArray+AutoLayout.h"
#import "NSLayoutConstraint+AutoLayout.h"

@implementation NSArray (AutoLayout)

- (void)ad_installConstraints {
    [NSLayoutConstraint ad_installConstraints:self];
}

- (void)ad_removeConstraints {
    [NSLayoutConstraint ad_removeConstraints:self];
}

@end
