//
// Created by Anton Dobkin on 15.02.15.
// Copyright (c) 2015 Browsec. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSArray (AutoLayout)
- (void)ad_installConstraints;
- (void)ad_removeConstraints;
@end
