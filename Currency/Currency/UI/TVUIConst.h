//
//  TVUIConst.h
//  Currency
//
//  Created by Valentin Titov on 08.07.16.
//  Copyright © 2016 Valentin Titov. All rights reserved.
//

#ifndef TVUIConst_h
#define TVUIConst_h

#define kBackgroundMinColor [UIColor colorWithRed:52.0f/255.0f green:137.0f/255.0f blue:219.0f/255.0f alpha:1.0f]
#define kBackgroundMaxColor [UIColor colorWithRed:1.0f/255.0f green:83.0f/255.0f blue:180.0f/255.0f alpha:1.0f]
#define kDarkBackgroundColor [UIColor colorWithRed:0.0f green:0.0f blue:0.0f alpha:0.25f]
#define kDefaultFontColor [UIColor whiteColor]
#define kDefaultBalanceColor [UIColor lightGrayColor]
#define kWarningBalanceColor [UIColor redColor]
static NSString* const kDefaultFontFamily = @"AppleSDGothicNeo-Light";
static CGFloat const kBigFontSize = 36.0f;
static CGFloat const kDefaultFontSize = 14.0f;
static CGFloat const kDefaultPadding = 10.0f;
static CGFloat const kDefaultHeight = 40.0f;
static CGFloat const kDefaultBorderWidth = 2.0f;
static CGFloat const kDefaultBorderCornerRadius = 10.0f;

static NSInteger const roundCount = 30;
static CGFloat const kMinRoundSide = 5.0f;
static CGFloat const kMaxRoundSide = 10.0f;
#endif /* TVUIConst_h */
