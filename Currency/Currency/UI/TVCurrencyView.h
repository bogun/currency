//
//  TVCurrencyView.h
//  Currency
//
//  Created by Valentin Titov on 06.07.16.
//  Copyright © 2016 Valentin Titov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TVView.h"
@class  TVCurrencyView,TVIndex,TVCurrencyCell,TVPageControl;

/** Currency view dataSource protocol */
@protocol TVCurrencyViewDataSource <NSObject>

@required

/*  Call for get number of currency sections. In this version all time you must set value: "2"
 *  @param currencyView An instance of TVCurrencyView
 *  @return number of sections (2)
 */
- (NSInteger) numbersOfSectionsInView:(TVCurrencyView*)currencyView;
/*  Call for get number of currency pages.
 *  @param currencyView An instance of TVCurrencyView
 *  @param section An number of section which has pages
 *  @return number of pages
 */
- (NSInteger) currencyView:(TVCurrencyView*)currencyView
     numberOfRowsInSection:(NSInteger)section;
/*  Call for get currency cell.
 *  @param currencyView An instance of TVCurrencyView
 *  @param index An index which contain information about section and page
 *  @return instance of new currency cell
 */
- (TVCurrencyCell*) currencyView:(TVCurrencyView*)currencyView
               cellForRowAtIndex:(TVIndex*) index;
/*  Call for display updated currency cell.
 *  @param currencyCell A cell for display
 *  @param index An index of cell
 */
- (void) currencyCell:(TVCurrencyCell*)currencyCell
   displayCellAtIndex:(TVIndex*)index;

@end


@interface TVCurrencyView : TVView

@property (strong, nonatomic) id <TVCurrencyViewDataSource> dataSource;
/* Update all elements position. Called when keyboard showed or hidden */
- (void) updateElementsPosition;
/* Set page for current currency pair
 * @param pageFrom A new number which will set for currency-from
 * @param pageTo A new number which will set for currency-to
 */
- (void) setCurrencyPageFrom:(NSInteger)pageFrom
              currencyPageTo:(NSInteger)pageTo;
/* Get number of current page for currency-from
 * @return Number of current page
 */
- (NSInteger) currentPageCurrencyFrom;
/* Get number of current page for currency-to
 * @return Number of current page
 */
- (NSInteger) currentPageCurrencyTo;
@end

@interface TVIndex : NSObject
/* Number of sections */
@property(assign, nonatomic) NSInteger section;
/* Number of rows */
@property(assign, nonatomic) NSInteger row;

- (instancetype)initWithSection:(NSInteger)section
                            row:(NSInteger)row;

@end
