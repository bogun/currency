//
//  TVView.h
//  Currency
//
//  Created by Valentin Titov on 08.07.16.
//  Copyright © 2016 Valentin Titov. All rights reserved.
//

#import <UIKit/UIKit.h>
/* Abstract class */
@interface TVView : UIView

/** Reset all constraint for view
 *  @param view A view which constraint need delete
 */
- (void) resetConstraintForView:(UIView*)view;

@end
