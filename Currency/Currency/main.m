//
//  main.m
//  Currency
//
//  Created by Valentin Titov on 06.07.16.
//  Copyright © 2016 Valentin Titov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
