//
//  TVCurrencyController.m
//  Currency
//
//  Created by Valentin Titov on 06.07.16.
//  Copyright © 2016 Valentin Titov. All rights reserved.
//

#import "TVCurrencyController.h"
#import "TVPopUpController.h"
#import "TVCurrencyManager.h"
#import "TVCurrencyView.h"
#import "TVCurrencyCell.h"
#import "TVBarButtonItem.h"
#import "TVRateField.h"
#import "TVXMLParser.h"
#import "Reachability.h"

#import "TVUIConst.h"
#import "TVDataSourceConst.h"


@interface TVCurrencyController () <TVXMLParserDataSource, TVCurrencyViewDataSource, TVPopUpControllerDelegate> {
    
    TVCurrencyView *_currencyView;
    TVPopUpController *_popUpController;
    
    TVXMLParser *_xmlParser;
    NSArray* _currencyList;
    TVCurrencyManager *_currencyManager;
    NSDictionary *_userBalanceInfo;
    
    UITextField *_valueFrom;
    UITextField *_valueTo;
    
    TVBarButtonItem *_cancelBarItem;
    TVBarButtonItem *_exchangeBarItem;
    
    NSTimer *_timer;
}

@property (nonatomic) Reachability *internetReachability;

@end

@implementation TVCurrencyController


#pragma mark - initialization elements
- (void)viewDidLoad {
    [super viewDidLoad];
    [self requestUserBalanceInfo];
    [self initCurrencyView];
    [self initReachability];
    [self checkInternetConnection:nil];
    [self addObservers];
    [self initParser];
    [self initTimer];
    [self requestCurrency];
    [self setNavigationBarStyle];
}

- (void) initReachability {
    self.internetReachability = [Reachability reachabilityForInternetConnection];
    [self.internetReachability startNotifier];
}

- (void) addObservers {
    NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
    [nc addObserver:self
           selector:@selector(keyboardWillShow:)
               name:UIKeyboardWillShowNotification
             object:nil];
    [nc addObserver:self
           selector:@selector(keyboardWillHide:)
               name:UIKeyboardWillHideNotification
             object:nil];
    [nc addObserver:self
           selector:@selector(checkInternetConnection:)
               name:kReachabilityChangedNotification object:nil];
}

- (void) initParser {
    _xmlParser = [[TVXMLParser alloc] initWithCurrency:[_currencyManager userCurrency]];
    _xmlParser.dataSource = self;
}

- (void) initTimer {
    _timer = [NSTimer scheduledTimerWithTimeInterval:kUpdateCurrencyTimeInterval
                                              target:self
                                            selector:@selector(requestCurrency)
                                            userInfo:nil
                                             repeats:YES];
}


- (void) initCurrencyView {
    _currencyView = [[TVCurrencyView alloc] initWithFrame:self.view.frame];
    [self.view addSubview: _currencyView];
    _currencyView.dataSource = self;
    [self showNavigationBarItems];
}

#pragma mark - load info to interface
- (void) showDefaultPage {
    [_currencyManager setCurrencyRateWithInfo:_currencyList[0]];
    [self updateCurrencyCells];
    [_popUpController updatePopUpControllerWithCurrencyList:_currencyList];
}

- (void) updateCurrencyCells {
    NSDictionary *pairInfo = [_currencyManager currencyPairInfo];
    [self showCurrencyPair:pairInfo];
}


#pragma mark - Setting UINavigationBar
- (void) setNavigationBarStyle {
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new]
                                                  forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.shadowImage = [UIImage new];
    self.navigationController.navigationBar.translucent = YES;
}

- (void) showNavigationBarItems {
    [self showCancelItem];
    [self showExchangeItem];
    [self initializationPopUpController];
}

- (void) showCancelItem {
    _cancelBarItem = [[TVBarButtonItem alloc] initWithTitle:@"Отменить" style:UIBarButtonItemStylePlain target:self action:@selector(cancelExchange)];
    [self.navigationItem setLeftBarButtonItem:_cancelBarItem];
    _cancelBarItem.enabled = NO;
}

- (void) showExchangeItem {
    _exchangeBarItem = [[TVBarButtonItem alloc] initWithTitle:@"Exchange" style:UIBarButtonItemStylePlain target:self action:@selector(exchange)];
    [self.navigationItem setRightBarButtonItem:_exchangeBarItem];
    _exchangeBarItem.enabled = NO;
}

- (void) initializationPopUpController {
    _popUpController = [[TVPopUpController alloc] init];
    _popUpController.presentView = _currencyView;
    _popUpController.delegate = self;
    [_popUpController showRateFieldWithCurrencyRateInfo:_currencyList[0]];
    self.navigationItem.titleView = _popUpController.rateField;
}

#pragma mark - Request methods
- (void) requestCurrency {
    [_xmlParser startParse];
}

- (void) requestUserBalanceInfo {
    _currencyManager = [TVCurrencyManager currencyManager];
    _userBalanceInfo = [_currencyManager userBalanceInfo];
}


#pragma mark - TVXMLParserDataSource methods
- (void)didLoadCurrencyRate:(NSArray *)currencyRate {
    if (!_currencyList) {
        _currencyList = [_currencyManager fullInfoForCurrencyRates:currencyRate];
        [self showDefaultPage];
    } else {
        _currencyList = [_currencyManager fullInfoForCurrencyRates:currencyRate];
        [_currencyManager updateCurrencyRateFrom:_currencyManager.currencyFrom];
        [_currencyManager updateCurrencyRateTo:_currencyManager.currencyTo];
        [self updateCurrencyCells];
    }
}


#pragma mark - TVCurrencyViewDataSource methods

- (NSInteger) numbersOfSectionsInView:(TVCurrencyView*)currencyView {
    return kCurrencyViewCellsCount;
}

- (NSInteger) currencyView:(TVCurrencyView*)currencyView
     numberOfRowsInSection:(NSInteger)section {
    return [_userBalanceInfo allKeys].count;
}

- (TVCurrencyCell*) currencyView:(TVCurrencyView*)currencyView
               cellForRowAtIndex:(NSIndexPath*) indexPath {
    
    NSDictionary *currencyInfo = nil;
    NSString *currencyTitle = nil;
    TVCurrencyCell *currencyCell = [[TVCurrencyCell alloc] init];
    if (indexPath.section == 0) {
        currencyInfo = _userBalanceInfo[kDefaultCurrencyFrom];
        currencyTitle = kDefaultCurrencyFrom;
        _valueFrom = (UITextField*)currencyCell.currencyValue;
        [_valueFrom addTarget:self
                      action:@selector(valueFromDidChange:)
            forControlEvents:UIControlEventEditingChanged];
    } else if (indexPath.section == 1) {
        currencyInfo = _userBalanceInfo[kDefaultCurrencyTo];
        currencyTitle = kDefaultCurrencyTo;
        [currencyCell setDarkBackground];
        [self setRateForCurrencyCell:currencyCell];
        _valueTo = (UITextField*)currencyCell.currencyValue;
        [_valueTo addTarget:self
                       action:@selector(valueToDidChange:)
             forControlEvents:UIControlEventEditingChanged];
    }
    [currencyCell setCurrencyTitle:currencyTitle
                           balance:[currencyInfo[@"balance"] floatValue]
                            symbol:currencyInfo[@"symbol"]];
    
    [self checkValidBalance:[currencyInfo[@"balance"] floatValue]
                    forCell:currencyCell];
    return currencyCell;
}

- (void) currencyCell:(TVCurrencyCell*)currencyCell
   displayCellAtIndex:(TVIndex*)index {
    if ([self.internetReachability currentReachabilityStatus] != NotReachable) {
        NSString *currency = [_userBalanceInfo allKeys][index.row];
        NSDictionary *currencyInfo = _userBalanceInfo[currency];
        [currencyCell setCurrencyTitle:currency
                               balance:[currencyInfo[@"balance"] floatValue]
                                symbol:currencyInfo[@"symbol"]];
        [self checkValidBalance:[currencyInfo[@"balance"] floatValue]
                        forCell:currencyCell];
        if (index.section == 0) {
            [_currencyManager updateCurrencyRateFrom:currency];
            [self valueToDidChange:_valueTo];
        } else if (index.section == 1) {
            [_currencyManager updateCurrencyRateTo:currency];
            [self setRateForCurrencyCell:currencyCell];
            [self valueFromDidChange:_valueFrom];
        }
        [_popUpController.rateField setTextWithCurrencyRateInfo:[_currencyManager currencyPairInfo]];
        [self checkAvailableExchangeOperation];
        [self checkExistsExchangeOperation];
    }
}

- (void) setRateForCurrencyCell:(TVCurrencyCell*)cell {
    [cell setRateValueFrom:_currencyManager.rateValueFrom
                symbolFrom:_currencyManager.symbolTo
               rateValueTo:_currencyManager.reverseValue
                  symbolTo:_currencyManager.symbolFrom];
}


#pragma mark - TVPopUpControllerDelegate methods
- (void) showCurrencyPair:(NSDictionary*)pairInfo {
    [_currencyManager setCurrencyRateWithInfo:pairInfo];
    NSInteger pageFrom = [[_userBalanceInfo allKeys] indexOfObject:pairInfo[@"currencyFrom"]];
    NSInteger pageTo = [[_userBalanceInfo allKeys] indexOfObject:pairInfo[@"currencyTo"]];
    [_currencyView setCurrencyPageFrom:pageFrom
                        currencyPageTo:pageTo];
}


#pragma mark - online calculate methods methods

- (void) valueFromDidChange:(UITextField*) textField {
    [self calculateValueToWithValueFrom:[textField.text floatValue]];
}

- (void) valueToDidChange:(UITextField*) textField {
    [self calculateValueFromWithValueTo:[textField.text floatValue]];
}

- (void) calculateValueToWithValueFrom:(CGFloat)userValueFrom {
    [_valueTo setText:[NSString stringWithFormat:@"%0.2f", [_currencyManager calculateValueToWithValueFrom:userValueFrom]]];
    [self checkAvailableExchangeOperation];
}

- (void) calculateValueFromWithValueTo:(CGFloat)userValueTo {
    [_valueFrom setText:[NSString stringWithFormat:@"%0.2f", [_currencyManager calculateValueFromWithValueTo:userValueTo]]];
    [self checkAvailableExchangeOperation];
}


#pragma mark - Exchage methods

- (void) exchange {
    [self.view endEditing:YES];
    //save last operation
    [_currencyManager saveExchangeOperationWithDeductValue:[_valueFrom.text floatValue]];
    [self updateBalanceOfCurrencyFrom];
    [self updateBalanceOfCurrencyTo];
    [self updateCurrencyCells];
    [self resetUserValue:_valueFrom];
    [self resetUserValue:_valueTo];
}

- (void) updateBalanceOfCurrencyFrom {
    NSInteger currentPage = [_currencyView currentPageCurrencyFrom];
    CGFloat value = [_valueFrom.text floatValue];
// The value will deduct from user balance
    if (value > 0) {
        value *= -1;
    }
    [_currencyManager updateBalanceForPage:currentPage
                                 withValue:value];
}

- (void) updateBalanceOfCurrencyTo {
    NSInteger currentPage = [_currencyView currentPageCurrencyTo];
    CGFloat value = [_valueTo.text floatValue];
    [_currencyManager updateBalanceForPage:currentPage
                                 withValue:value];
}

- (void) checkAvailableExchangeOperation {
    CGFloat balance = [self getBalanceOfCurrencyFrom];
    CGFloat valueFrom = fabsf([_valueFrom.text floatValue]);
    if (valueFrom <= balance &&
        valueFrom != 0.0f &&
        balance >= kMinExchangeBalance) {
        _exchangeBarItem.enabled = YES;
    } else {
        _exchangeBarItem.enabled = NO;
    }
}

- (void) checkExistsExchangeOperation {
    if ([_currencyManager hasExchangeOperationCurrentCurrencyPair]) {
        _cancelBarItem.enabled = YES;
    } else {
        _cancelBarItem.enabled = NO;
    }
}

- (void) cancelExchange {
    [_currencyManager cancelLastExchangeOperation];
    [self updateCurrencyCells];
}

- (CGFloat) getBalanceOfCurrencyFrom {
    NSInteger currentPage = [_currencyView currentPageCurrencyFrom];
    NSString *currency = [_userBalanceInfo allKeys][currentPage];
    NSDictionary *currencyInfo = _userBalanceInfo[currency];
    return [currencyInfo[@"balance"] floatValue];
}


#pragma mark - Show/Hide Keyboard
- (void)keyboardWillShow:(NSNotification *)notification {
    NSDictionary *userInfo = notification.userInfo;
    NSTimeInterval duration = [userInfo[UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    UIViewAnimationCurve curve = [userInfo[UIKeyboardAnimationCurveUserInfoKey] integerValue];
    
    CGRect keyboardFrameEnd = [userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue];
    keyboardFrameEnd = [self.view convertRect:keyboardFrameEnd fromView:nil];
    
    [UIView animateWithDuration:0.0f delay:duration options:UIViewAnimationOptionBeginFromCurrentState | curve animations:^{
        _currencyView.frame = CGRectMake(0, 0, self.view.frame.size.width, keyboardFrameEnd.origin.y);
        [_currencyView updateElementsPosition];
    } completion:nil];
}

- (void)keyboardWillHide:(NSNotification *)notification {
    NSDictionary *userInfo = notification.userInfo;
    NSTimeInterval duration = [userInfo[UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    UIViewAnimationCurve curve = [userInfo[UIKeyboardAnimationCurveUserInfoKey] integerValue];
    [UIView animateWithDuration:0.0f delay:duration options:UIViewAnimationOptionBeginFromCurrentState | curve animations:^{
        _currencyView.frame = self.view.frame;
        [_currencyView updateElementsPosition];
    } completion:nil];
}


#pragma mark - Touch events
- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [self.view endEditing:YES];
    [self setValueFromLessThenZero];
    [_popUpController touchesBegan:touches withEvent:event];
}

- (void) setValueFromLessThenZero {
    if ([_valueFrom.text floatValue] > 0) {
        [_valueFrom setText:[NSString stringWithFormat:@"%0.2f",[_valueFrom.text floatValue]]];
    }
}

#pragma mark - Reachabilyty methods

- (void) checkInternetConnection:(NSNotification*) notification {
    if ([self.internetReachability currentReachabilityStatus] == NotReachable) {
        NSLog(@"Lost internet connection");
        [self showAlertWithTitle:@"Warning!"
                         meesage:@"Internet is not available now. Please, check connection"];
    } else {
        [self requestCurrency];
        NSLog(@"Internet connection established");
    }
}

#pragma mark - Dealloc

- (void) dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    _xmlParser = nil;
    _currencyView = nil;
}


#pragma mark - helpful methods

- (void) checkValidBalance:(CGFloat)balance
                   forCell:(TVCurrencyCell*)cell {
    if ([self isValidBalance:balance]) {
        [cell  setDefaultBalanceStyle];
    } else {
        [cell setWrongBalanceStyle];
    }
}

- (BOOL) isValidBalance:(CGFloat) balance {
    if (balance >= kMinExchangeBalance) {
        return YES;
    }
    return NO;
}

- (void) showAlertWithTitle:(NSString*)title
                    meesage:(NSString*) message {
    [[[UIAlertView alloc] initWithTitle:title
                                message:message
                               delegate:self
                      cancelButtonTitle:@"Ok"
                      otherButtonTitles: nil] show];
}

- (void) resetUserValue:(UITextField*) userValue {
    [userValue setText:@"0.00"];
}

@end
