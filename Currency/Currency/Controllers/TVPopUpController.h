//
//  TVPopUpController.h
//  Currency
//
//  Created by Valentin Titov on 13.07.16.
//  Copyright © 2016 Valentin Titov. All rights reserved.
//

#import <UIKit/UIKit.h>
@class TVRateField, TVPopUpView;

@protocol TVPopUpControllerDelegate <NSObject>

- (void) showCurrencyPair:(NSDictionary*)pairInfo;

@end


@interface TVPopUpController : NSObject
/** View, where will be place popUpView */
@property(strong, nonatomic) UIView *presentView;
/** Field with current currency pair rate */
@property(strong, nonatomic) TVRateField *rateField;

/** Delegate of TVPopUpControllerDelegate protocol */
@property (strong, nonatomic) id <TVPopUpControllerDelegate> delegate;

/** Will update content of popUp view
 *  @param currencyList An array containing info wich will display on popup view
 */
- (void) updatePopUpControllerWithCurrencyList:(NSArray *)currencyList;
/** Initialization property rateField and set value with spwcial format
 *  @param info A dictionary containing parameters, wich will convert to string and set to rateField
 */
- (void) showRateFieldWithCurrencyRateInfo:(NSDictionary*)info;

/** Pass touch event from view controller. It's need to hide popUp view
 *  @param touches A NSSet containing toches
 *  @param event An event containing event info
 */
- (void) touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event ;

@end
