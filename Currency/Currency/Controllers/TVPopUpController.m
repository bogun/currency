//
//  TVPopUpController.m
//  Currency
//
//  Created by Valentin Titov on 13.07.16.
//  Copyright © 2016 Valentin Titov. All rights reserved.
//

#import "TVPopUpController.h"
#import "TVCurrencyManager.h"
#import "TVPopUpView.h"
#import "TVPopUpViewCell.h"
#import "TVRateField.h"
#import "TVUIConst.h"
#import "Reachability.h"

@interface TVPopUpController () <UITableViewDataSource, UITableViewDelegate> {
    NSArray *_currencyList;
    TVPopUpView *_tableView;
}

@property (nonatomic) Reachability *internetReachability;

@end

@implementation TVPopUpController


- (instancetype)init {
    self = [super init];
    if (self) {
        [self initializationRateField];
        [self initReachability];
    }
    return self;
}

- (void) initializationRateField {
    self.rateField = [[TVRateField alloc] init];
    UIButton *showRates = (UIButton*)_rateField.showRates;
    [showRates addTarget:self action:@selector(presentPopUpController) forControlEvents:UIControlEventTouchUpInside];
}

- (void) initReachability {
    self.internetReachability = [Reachability reachabilityForInternetConnection];
}

- (void) updatePopUpControllerWithCurrencyList:(NSArray *)currencyList {
    _currencyList = currencyList;
    [_tableView reloadData];
}

- (void) showRateFieldWithCurrencyRateInfo:(NSDictionary*)info {
    [self.rateField  setFrame:CGRectMake(0.0f, 0.0f, 150.0f, kDefaultHeight)];
    [self.rateField setTextWithCurrencyRateInfo:_currencyList[0]];
}

- (void) presentPopUpController {
    if (!_tableView) {
        _tableView = [[TVPopUpView alloc] initWithFrame:CGRectMake(self.rateField.frame.origin.x, CGRectGetMaxY(self.rateField.frame) + [[UIApplication sharedApplication] statusBarFrame].size.height, self.rateField.frame.size.width, kDefaultHeight*4)];
        _tableView.dataSource = self;
        _tableView.delegate = self;
    }
    if ([self.internetReachability currentReachabilityStatus] != NotReachable && !_tableView.isShown) {
        [self showPopUpView];
    } else {
        [self hidePopUpView];
    }
}

- (void) touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    UITouch *touch = [touches anyObject];
    if (![touch.view isEqual:_tableView]) {
        [self hidePopUpView];
    }
}

- (void) showPopUpView {
    [self.presentView addSubview:_tableView];
    _tableView.isShown = YES;
}

- (void) hidePopUpView {
    [_tableView removeFromSuperview];
    _tableView.isShown = NO;
}

#pragma mark - UITableViewDataSource methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _currencyList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Cell";
    TVPopUpViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[TVPopUpViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    NSDictionary *currencyInfo = _currencyList[indexPath.row];
    [cell setTextWithInfo:currencyInfo];
    return cell;
}

#pragma mark - UITableViewDelegate methods

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    [self hidePopUpView];
    NSDictionary *currencyInfo = _currencyList[indexPath.row];
    [[TVCurrencyManager currencyManager] setCurrencyRateWithInfo:currencyInfo];
    [self.rateField setTextWithCurrencyRateInfo:currencyInfo];
    [self.delegate showCurrencyPair:currencyInfo];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return kDefaultHeight;
}


@end
